#include <cmath>
// Note we need to include the header here so that this source file can access
// to the Quad class definition.
#include "quad.h"
using namespace std;

double Quad::root1() {
    // Return the larger root of a quadratic equation.

    double root;

    root = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root = root / (2.0 * a);
    return root;
}

double Quad::root2() {
    // Return the smaller root of a quadratic equation.

    double root;

    root = -b - sqrt(pow(b, 2) - 4.0 * a * c);
    root = root / (2.0 * a);
    return root;
}
