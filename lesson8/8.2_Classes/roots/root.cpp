// An example of the use of a class that is defined in a separate file using
// a header file.
#include <iostream>
// Again we must include the header file here.
#include "quad.h"
using namespace std;

int main() {
    Quad eqn;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> eqn.a;
    cin >> eqn.b;
    cin >> eqn.c;

    cout << eqn.root1() << " " << eqn.root2() << endl;

    return 0;
}
