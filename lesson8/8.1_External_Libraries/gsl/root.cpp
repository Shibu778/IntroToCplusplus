// Code to show how to call an external library (GSL quadratic root solver).
#include <iostream>
// This loads the interface to the various functions for working with
// polynomials provided by gsl.
#include <gsl/gsl_poly.h>
using namespace std;

int main() {
    int nroots;
    double a, b, c, root1, root2;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> a;
    cin >> b;
    cin >> c;

    // Now we can use a gsl function directly in our code. This function takes
    // coefficients of a quadratic equation, along with two pointers to
    // doubles that will be used to store the roots, and returns an integer
    // indicating the number of roots found. Note we need to add
    // '-lgsl -lgslcblas' to LDFLAGS in our Makefile.
    nroots = gsl_poly_solve_quadratic(a, b, c, &root1, &root2);
    if (nroots == 2) {
      cout << " Two real roots found: " << root1 << " " << root2 << endl;
    } else if (nroots == 1) {
      cout << " One real root found: " << root1 << endl;
    } else {
      cout << " No real roots found." << endl;
    }

    return 0;
}
