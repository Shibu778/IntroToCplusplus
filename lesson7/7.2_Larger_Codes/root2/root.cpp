// An example of the use of a function that is defined in a separate file
// using a header file.
#include <iostream>
// We have this extra include line to add the header file where the interface
// is defined.
#include "func.h"
using namespace std;

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}
