// Example showing the use of pointers.
#include <iostream>
using namespace std;

int main() {
    int x;
    int *p;

    x = 42;
    p = &x;

    cout << "Value of x: " << x << endl;
    cout << "Address of x: " << &x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << *p << endl;

    return 0;
}
