// Examples of the use of functions that return values and have arguments.
#include <iostream>
using namespace std;

// Here we define a function of type "bool", this means it will return true or
// false. We also state that the function takes one argument: an integer
// called n. All arguments and their types must be declared in this manner.
bool divisible_by_3(int n) {
    if (n % 3 == 0) {
        return true;
    } else {
        return false;
    }
}

int double_x(int x) {
    // Note C++ passes arguments by value. This means that variables are
    // copied and are local to the function.
    x = x * 2;
    return x;
}

int main() {
    int user_n;

    cout << "Enter an integer: ";
    cin >> user_n;

    // Note we can use bool type variables directly in the if statement
    // test (i.e. we don't need to test they are equal to true).
    if (divisible_by_3(user_n)) {
        cout << user_n << " is divisible by 3." << endl;
    } else {
        cout << user_n << " is not divisible by 3." << endl;
    }

    cout << "The value returned by double_x is " << double_x(user_n) << endl;
    cout << "The value of the input number is now " << user_n << endl;

    return 0;
}
