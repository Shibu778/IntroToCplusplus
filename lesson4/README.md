Introduction to C++: Lesson 4
=============================

Topics
------

1. [Functions](#41-functions),
2. [Pointers](#42-pointers)

4.1 Functions
-------------

Functions allow us to define pieces of code that can be reused as needed.
Examples are given the `4.1_Functions` directory. First take a look at
`functions1.cpp`:

```c++
// Examples of the use of functions.
// These allow a code to be broken down into pieces which can be reused as
// needed rather than putting everything in main.
#include <iostream>
#include <cmath>
using namespace std;

// Here we define a function of type "void". This type can be used for
// functions which do not return a value.
void loop_over_10() {
    int i;
    for (i = 0; i < 10; ++i) {
        cout << i << " " << pow(double(i), 2) << endl;
    }
}

int main() {
    // Now we can call this function where ever we like without needing to
    // rewrite the for loop code.
    loop_over_10();
    cout << "Repeating loop." << endl;
    loop_over_10();

    return 0;
}
```

Functions of other types will return values we can use directly. Functions
can also take arguments. Code showing both of these features is given in
`functions2.cpp`:

```c++
// Examples of the use of functions that return values and have arguments.
#include <iostream>
using namespace std;

// Here we define a function of type "bool", this means it will return true or
// false. We also state that the function takes one argument: an integer
// called n. All arguments and their types must be declared in this manner.
bool divisible_by_3(int n) {
    if (n % 3 == 0) {
        return true;
    } else {
        return false;
    }
}

int double_x(int x) {
    // Note C++ passes arguments by value. This means that variables are
    // copied and are local to the function.
    x = x * 2;
    return x;
}

int main() {
    int user_n;

    cout << "Enter an integer: ";
    cin >> user_n;

    // Note we can use bool type variables directly in the if statement
    // test (i.e. we don't need to test they are equal to true).
    if (divisible_by_3(user_n)) {
        cout << user_n << " is divisible by 3." << endl;
    } else {
        cout << user_n << " is not divisible by 3." << endl;
    }

    cout << "The value returned by double_x is " << double_x(user_n) << endl;
    cout << "The value of the input number is now " << user_n << endl;

    return 0;
}
```

An example with a function that takes several arguments is given in
`functions3.cpp`. This example also illustrates how we can move the
function definition to a point following the definition of the main function,
so long as we define the interface before main (this gives the type of the
function and its arguments):

```c++
// An example of the use of a function that returns values and has
// arguments, and is definied following main.
#include <iostream>
#include <cmath>
using namespace std;

// Here we define a function of type "double", this means it will return
// a double precision real number. And this function takes three doubles as
// arguments.
double quadratic_root1(double, double, double);
// This time we just define the interface here, and give the full function
// definition after the main function.

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}

double quadratic_root1(double a, double b, double c) {
    // Return the larger root of a quadratic equation.

    double root1;

    root1 = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root1 = root1 / (2.0 * a);
    return root1;
}
```

### [Exercises](../exercises/README.md#41-functions)

Please complete the exercises listed in
[section 4.1](../exercises/README.md#41-functions) of the file
`exercises/README.md`.

4.2 Pointers
------------

The data associated with a variable is stored at a particular point in
memory. C and C++ use a type of variable called a pointer that refers to
the address in memory where the data of another variable is stored.

Two operators are used to work with pointers:

- **The address operator `&`** returns the address of a variable: the place
  in memory where its value is stored.
    - `&x` is the address of the object `x`.
- **The dereference operator `*`** returns the value of the variable which the
  pointer points to.
    - If `p` is the address of `x`, `*p` gives the value of `x`.

Examples showing how these operators are used are given the `4.2_Pointers`
directory. First `pointers1.cpp` covers the basic operations:

```c++
// Example showing the use of pointers.
#include <iostream>
using namespace std;

int main() {
    int x;
    int *p;

    x = 42;
    p = &x;

    cout << "Value of x: " << x << endl;
    cout << "Address of x: " << &x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << *p << endl;

    return 0;
}
```

How pointers can be used to manipulate data is shown in `pointers2.cpp`.

```c++
// Example showing further use of pointers to manipulate the data associated
// with a variable.
#include <iostream>
using namespace std;

int main() {
    int x;
    int *p;

    x = 42;
    p = &x;

    cout << "Value of x: " << x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << * p << endl;

    x += 1; // x is now 43
    cout << "Value of x: " << x << endl;
    cout << "Dereferencing p: " << * p << endl;

    *p += 1; // x is now 44
    cout << "Value of x: " << x << endl;
    cout << "Dereferencing p: " << * p << endl;

    // THIS CHANGES WHERE p POINTS TO AND MAY CAUSE A SEGMENTATION FAULT!
    // DON'T DO THIS IN YOUR OWN CODES!
    p += 1;
    cout << "Value of x: " << x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << *p << endl;

    return 0;
}
```

We can also have functions which take pointers as arguments and use these to
manipulate the value of variables in the main code. This is shown in
`pointers3.cpp`:

```c++
// Example showing how pointers can manipulate the data associated
// with a variable in a function.
#include <iostream>
using namespace std;

// This function takes a pointer to an integer as an argument and squares
// the value assocated with it.
void square(int *n) {
    *n = *n * *n;
}

int main() {
    int x;

    cout << "Enter an integer: ";
    cin >> x;

    square(&x);

    cout << "The value has been squared: " << x << endl;

    return 0;
}
```

### [Exercises](../exercises/README.md#42-functions)

Please complete the exercises listed in
[section 4.2](../exercises/README.md#42-functions) of the file
`exercises/README.md`.

