Introduction to C++
===================

Preface
-------

This is a short course intended to give incoming TSM CDT Masters students a
firm foundation in programming C++ as they will need to be able to create their
own programs in subsequent courses. This course is largely modelled on James
Spencer's previous Intro to C++ course.

It is assumed that students will have already taken the Introduction to Linux
course at https://gitlab.com/eamonn.murray/IntroToLinux prior to this, but
otherwise no programming knowledge is assumed. No doubt students with some
programming experience will find this quite easy; if so please do work through
the exercises in any case and consider this opportunity to brush up on your
skills. If you want an additional challenge, consider working through the
problems given at <https://projecteuler.net>.

One can either use this README file or work through the annotated examples in
the directories in the order in which they are numbered. I suggest either
cloning the course with git to a local workspace
(`git clone https://gitlab.com/eamonn.murray/IntroToCplusplus.git`), or
forking it directly with the "Fork" button if you have a GitLab account. You
can then use git to track your changes as you work through the exercises. Note
that the file `.gitignore` has already been set to ignore files with the `.o`
or `.x` extension. If you have cloned the repository from here, you won't have
permission to `git push`. You can set your own remote, to e.g. your own GitLab
repository, by typing
`git remote set-url origin git@gitlab.com:username/projectname`.

If you don't wish to use git, you can also download this file and all the
examples as e.g.
https://gitlab.com/eamonn.murray/IntroToCplusplus/repository/archive.tar.bz2 to
get an archive of the latest version.

The course is heavily example driven and also makes use of "make" as introduced
in the first section. The example makefiles are set up to use the g++ compiler.
If you do not have this available, or prefer to use a different compiler,
simply edit the Makefiles.

This course is somewhat limited in scope. In particular it only briefly covers
some aspects of object-oriented programming and does not cover the template and
string manipulation facilities provided by C++. The main differences between
the C++ presented here and the standard C are how the IO, memory, pointers and
arrays are handled, and the inclusion of classes.

Additional Resources
--------------------

There are many excellent free resources available online such as:

- <http://www.learncpp.com>
- <http://www.cplusplus.com>
- <http://www.cppreference.com>

A list of recommended books is available at
<http://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list>.

Lessons
-------

1. [Lesson 1:](lesson1/README.md)
   [Compiling and Makefiles](lesson1/README.md#11-compiling-and-makefiles),
   [IO and Strings](lesson1/README.md#12-io-and-strings)
2. [Lesson 2:](lesson2/README.md)
   [Basic Types](lesson2/README.md#21-basic-types),
   [Mathematical Operations](lesson2/README.md#22-mathematical-operations)
3. [Lesson 3:](lesson3/README.md)
   [Conditions](lesson3/README.md#31-conditions),
   [Loops](lesson3/README.md#32-loops)
4. [Lesson 4:](lesson4/README.md)
   [Functions](lesson4/README.md#41-functions),
   [Pointers](lesson4/README.md#42-pointers)
5. [Lesson 5:](lesson5/README.md)
   [Static Arrays](lesson5/README.md#51-static-arrays),
   [Arguments to Main](lesson5/README.md#52-arguments-to-main)
6. [Lesson 6](lesson6/README.md)
   [Dynamic Arrays](lesson6/README.md#61-dynamic-arrays),
   [Working With Files](lesson6/README.md#62-working-with-files)
7. [Lesson 7:](lesson7/README.md)
   [Data Structures](lesson7/README.md#71-data-structures),
   [Working With Larger Codes](lesson7/README.md#72-working-with-larger-codes)
8. [Lesson 8:](lesson8/README.md)
   [Using External Libraries](lesson8/README.md#81-using-external-libraries),
   [Classes](lesson8/README.md#82-classes)
8. [Lesson 9:](lesson9/README.md)
   [Further Reading](lesson9/README.md#91-further-reading)

Exercises
---------

The exercises for each lesson, and instructions for completing them are
listed in the directory [exercises](exercises/README.md).

