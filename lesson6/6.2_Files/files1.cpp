// Example showing how to read a file.
#include <iostream>
#include <fstream>
// fstream contains functions to read and write files.
#include <string>

using namespace std;

int main() {

    string input_line;

    ifstream infile;
    // ifstream is used to open a file for reading.
    infile.open("example.dat");
    // This pair of commands can be combined and shortened to
    // ifstream infile("example.dat");

    // It's good to test that the file was successfully opened before
    // trying to work with it.
    if ( ! infile.is_open() ) {
        cout << "Error opening file." << endl;
	return 1;
	// Here we return 1 to indicate an error occurred.
    }

    // Here we use the getline function to read a line from the file to a
    // string. We can use this in a while loop to iterate over every line,
    // since getline will return false when it fails to read any more lines.
    while (getline(infile, input_line)) {
        cout << input_line << endl;
    }
    infile.close();

    return 0;
}
