# We can make more extensive use of variables in both the target and
# dependency lists to make a more general purpose Makefile. Here we make use
# of the $@ variable which resolves to the name of the target currently being
# processed, and the $< variable which contains the first dependency listed.

# While this may look quite complicated, it breaks down into setting variables
# defining the compiler, flags, source and executable names, and then listing
# a set of recipes to compile and put everything together.

# Use the g++ compiler
CC = g++

# Set the compiler flags. These turn on additional compiler warnings.
CFLAGS = -c -Wall -Wextra

# Set the linker flags.
LDFLAGS =

EXECUTABLE = hello.x
# Any other sources could be listed here too, separated by spaces.
SOURCES = hello.cpp
# This automatically generates a list of object names by substituting .cpp
# wih .o
OBJECTS = $(SOURCES:.cpp=.o)

all: $(SOURCES) $(EXECUTABLE)

# This says the executable depends on all the objects, so make will check
# whether all the objects are compiled first. And following this is the
# line defining how to link all the object files together. The $@ substitutes
# in the name of the target, so we could use $(EXECUTABLE) instead here.
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

# Now we define are rule to make an object file from a source file. The $<
# substitutes in the name of the dependency.
%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm $(EXECUTABLE) $(OBJECTS)
