// Example of the switch statement.
#include <iostream>
using namespace std;

int main()
{
    int i = 1;
    char c;

    // switch allows you to test a value against several different options.
    switch(i) {
        case 0 :
            cout << "i = 0" << endl;
            cout << "This won't be output." << endl;
            break;
            // The break statement exits the switch statement.
        case 1 :
            cout << "i = 1" << endl;
            break;
        default :
            // The default section is processed if no other case values match.
            cout << "i didn't equal any of the options." << endl;
            break;
    }

    cout << "Please enter y or n and press return: ";
    cin >> c;
    switch(c) {
        // Note switch statements won't work with strings by default as they
        // are not in the basic language (only through the standard library).
        case 'y' :
            cout << "You entered y." << endl;
            break;
        case 'n' :
            cout << "You entered n." << endl;
            break;
        default :
            cout << "You didn't enter y or n!" << endl;
            break;
    }

    return 0;
}
