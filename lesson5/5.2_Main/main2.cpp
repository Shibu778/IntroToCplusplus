// Example using numbers passed as arguments to main.
// Example usage:
// ./main2.x 8 10.234
#include <iostream>
#include <cstdlib> // provides atoi and atof
using namespace std;

int main(int argc, char **argv) {
    int i, j;
    double x;

    cout << "Number of arguments: " << argc << endl;
    for (i = 0; i < argc; ++i) {
        cout << "argument " << i << " = " << argv[i] << endl;
    }
    j = atoi(argv[1]); // Convert this argument to an integer.
    x = atof(argv[2]); // Convert this argument to a double.
    cout << j << " * 3 = " << j * 3 << endl;
    cout << x << " * 3 = " << x * 3 << endl;

    return 0;
}
