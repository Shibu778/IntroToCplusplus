// Example showing the use of arrays.
#include <iostream>
using namespace std;

int main() {
    double coords[3];
    // A static array of a given type is declared by adding the number of
    // elements in square brackets following the name.

    for (int i = 0; i <= 2; ++i) { // NB indexing starts from 0.
        coords[i] = 2 * i + 1;
        cout << "Component " << i << " of coords: " << coords[i] << endl;
    }

    // Note arrays are closely related to pointers.
    cout << "coords " << coords << endl;
    cout << "&coords[0] " << &coords[0] << endl; // same as coords!

    // An array is the first pointer in a consecutive series of pointers. Thus
    // coords[n] is equivalent to *(coords + n). coords + 1 is the address of
    // element number 1 in the coords array, coords + 2 the address of element
    // number 2 and so on.

    return 0;
}
