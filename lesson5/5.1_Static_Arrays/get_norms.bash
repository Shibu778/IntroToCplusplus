#!/bin/bash
#
# This is a simple bash script to repeatedly read lines from stdin
# and pass them to stdin of the arrays2.x c++ program.
#
# The file vectors.dat contains a list of vectors. You can pass it to this
# script by typing
# ./get_norms.bash < vectors.dat
# Then it will use the c++ program to calculate the norm of each listed
# vector.
#
# You can also run this script without redirecting the file into it:
# ./get_norms.bash
# in which case it will run in a loop, allowing you to repeatedly type
# in vectors until you signal to finish by typing ctrl+d.

while read line; do
    echo $line | ./arrays2.x
done < /dev/stdin
# Note bash allows us to redirect a file or channel into a loop.
# We could also write the file name directly here.
